import { Route } from "@angular/compiler/src/core";
import { Component, OnDestroy, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Store, select } from "@ngrx/store";
import { SocialAuthService } from "angularx-social-login";
import { AppState } from "../app.constant";
import { ReplaySubject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { resetAll } from '../user.action';

@Component({
  selector: "app-admin",
  templateUrl: "./admin.component.html",
  styleUrls: ["./admin.component.css"],
})
export class AdminComponent implements OnInit, OnDestroy {
  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  constructor(
    private authService: SocialAuthService,
    private router: Router,
    private store: Store
  ) {}
  names = [];
  ngOnInit() {
    this.store
      .pipe(
        select((state: AppState) => state.user.names),
        takeUntil(this.destroyed$)
      )
      .subscribe((response) => {
        this.names = response;
        console.log("Response: ", response);
      });
  }

  ngOnDestroy() {}

  logOut(): void {
    this.authService.signOut();
    this.onreset()
    this.router.navigate(["/"]);
  }
  showList(name) {
    this.router.navigate(["/userTask"], { state: { name: name } });
  }

  onreset() {
    this.store.dispatch(resetAll());
  }
}
