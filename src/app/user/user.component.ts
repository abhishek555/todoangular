import { Component, OnDestroy, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { select, Store } from "@ngrx/store";
import { SocialAuthService } from "angularx-social-login";
import { addTask, doneTask, resetAll } from "../user.action";
import { takeUntil } from "rxjs/operators";
import { UserState } from "../user.reducer";
import { ReplaySubject } from "rxjs";
import { AppState } from "../app.constant";

@Component({
  selector: "app-user",
  templateUrl: "./user.component.html",
  styleUrls: ["./user.component.css"],
})
export class UserComponent implements OnInit, OnDestroy {
  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  constructor(
    private router: Router,
    private authService: SocialAuthService,
    private store: Store
  ) {
    const email = this.router.getCurrentNavigation().extras.state.email;
    this.user = email;
    console.log(email);
  }

  tasks = [];
  user = "";
  ngOnInit() {
    this.store
      .pipe(
        select((state: AppState) => state.user.tasks),
        takeUntil(this.destroyed$)
      )
      .subscribe((response) => {
        this.tasks = response;
        let doneTask = [];
        let nondone = [];
        this.tasks.forEach((ele) => {
          if (ele.status == true) {
            doneTask.push(ele);
          } else {
            nondone.push(ele);
          }
        });
        this.tasks = [];
        nondone.forEach((ele) => {
          this.tasks.push(ele);
        });
        doneTask.forEach((ele) => {
          this.tasks.push(ele);
        });

        console.log("Response: ", response);
      });
  }

  ngOnDestroy(): void {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }

  add(task) {
    let taskList = JSON.parse(localStorage.getItem(this.user));
    if (taskList == null && task != "") {
      localStorage.setItem(
        this.user,
        JSON.stringify([{ task: task, status: false }])
      );
    } else if (taskList != null && task != "") {
      taskList = [{ task: task, status: false }, ...taskList];
      localStorage.setItem(this.user, JSON.stringify(taskList));
    }
    if (task != "") this.onAdd(task, false);
  }

  done(task) {
    this.onDone(task);
  }

  logOut(): void {
    this.authService.signOut();
    this.onreset()
    this.router.navigate(["/"]);
  }
  onAdd(task: string, status: boolean) {
    this.store.dispatch(addTask({ task: task, status: status }));
  }
  onDone(task: string) {
    let taskList = JSON.parse(localStorage.getItem(this.user));
    taskList.splice(
      taskList.findIndex((ele) => ele.task == task),
      1
    );
    taskList.push({ task: task, status: true });
    localStorage.setItem(this.user, JSON.stringify(taskList));
    this.store.dispatch(doneTask({ task: task, status: false }));
  }
  onreset() {
    this.store.dispatch(resetAll());
  }
}
