import { createAction, props } from "@ngrx/store";
import { TodoList } from "./user.reducer";

export const addTask = createAction(
  "[User Page] Add Task",
  props<{ task: string; status: boolean }>()
);

export const doneTask = createAction(
  "[User Page] Done Task",
  props<TodoList>()
);

export const addUser = createAction(
  "[Admin Page] Add User",
  props<{ name: string; isAdmin: boolean }>()
);

export const resetAll = createAction(
  "[Admin user Page] Reset All"
);
