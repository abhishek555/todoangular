import { Component, OnInit } from "@angular/core";
import { SocialAuthService } from "angularx-social-login";
import { GoogleLoginProvider } from "angularx-social-login";
import { Router } from "@angular/router";
import { Store } from "@ngrx/store";
import { addUser } from "../user.action";

@Component({
  selector: "app-signup",
  templateUrl: "./signup.component.html",
  styleUrls: ["./signup.component.css"],
})
export class SignupComponent {
  constructor(
    private authService: SocialAuthService,
    private router: Router,
    private store: Store
  ) {}

  user = "";
  isAdmin = false;
  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then((res) => {
      this.user = res.email;
    }).catch(err=>{
      console.log(err)
    })
  }

  signup(form) {
    if (form.admin == "admin") {
      this.isAdmin = true;
    }
    console.log(this.user, this.isAdmin);

    let userList = JSON.parse(localStorage.getItem("names"));
    if (userList == null) {
      let names = [{ name: this.user, isAdmin: this.isAdmin }];
      localStorage.setItem("names", JSON.stringify(names));
    } else {
      userList.push({ name: this.user, isAdmin: this.isAdmin });
      localStorage.setItem("names", JSON.stringify(userList));
      userList.forEach((ele) => {
        this.onAddUser(ele.name, ele.isAdmin);
      });
    }

    if (this.isAdmin) {
      this.router.navigate(["/admin"]);
    } else {
      this.router.navigate(["/user"], { state: { email: this.user } });
    }
  }

  onAddUser(name: string, isAdmin: boolean) {
    this.store.dispatch(addUser({ name: name, isAdmin: isAdmin }));
  }
}
