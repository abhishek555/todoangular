import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-user-task",
  templateUrl: "./user-task.component.html",
  styleUrls: ["./user-task.component.css"],
})
export class UserTaskComponent implements OnInit {
  name = "";
  constructor(private router: Router) {
    const name = this.router.getCurrentNavigation().extras.state.name;
    this.name = name;
    console.log(name);
  }
  tasks = [];
  ngOnInit() {
    this.tasks = JSON.parse(localStorage.getItem(this.name));
  }
}
