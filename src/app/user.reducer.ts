import { Action, createReducer, on } from "@ngrx/store";
import * as UserActions from "./user.action";

export interface UserState {
  tasks: Array<TodoList>;
  names: Array<name>;
}

export interface TodoList {
  task: string;
  status: boolean;
}
export interface name {
  name: string;
  isAdmin: boolean;
}

export const initialState: UserState = {
  tasks: [],
  names: [],
};
const userReducer = createReducer(
  initialState,
  on(UserActions.addTask, (state, { task, status }) => {
    console.log(task, status);
    return {
      ...state,
      tasks: [{ task: task, status: status }, ...state.tasks],
    };
  }),
  on(
    UserActions.doneTask,
    (state: UserState, newValue: TodoList): UserState => {
      let newTasks = [...state.tasks];
      newTasks.splice(
        newTasks.findIndex((ele) => ele.task == newValue.task),
        1
      );

      return {
        ...state,
        tasks: [...newTasks, { task: newValue.task, status: true }],
      };
    }
  ),
  on(UserActions.addUser, (state, { name, isAdmin }) => {
    return {
      ...state,
      names: [...state.names, { name: name, isAdmin: isAdmin }],
    };
  }),

  on(UserActions.resetAll, state => ({ tasks: [], names: [] })),
);

export function reducer(state = initialState, action: Action) {
  return userReducer(state, action);
}
