import { Component, OnInit } from "@angular/core";
import { SocialAuthService } from "angularx-social-login";
import { GoogleLoginProvider } from "angularx-social-login";
import { Router } from "@angular/router";
import { Store } from "@ngrx/store";
import { addTask, addUser } from "../user.action";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent {
  constructor(
    private authService: SocialAuthService,
    private router: Router,
    private store: Store
  ) {}
  user = "";
  logInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then((res) => {
      this.user = res.email;
      let names = JSON.parse(localStorage.getItem("names"));
      let isAdmin = false;
      names.forEach((ele) => {
        this.onAddUser(ele.name, ele.isAdmin);
        if (ele.name == this.user && ele.isAdmin == true) {
          isAdmin = true;
        }
      });

      let tasks = JSON.parse(localStorage.getItem(this.user));

      if (tasks != null) {
        tasks.forEach((ele) => {
          this.onAdd(ele.task, ele.status);
        });
      }

      if (isAdmin) {
        this.router.navigate(["/admin"]);
      } else {
        this.router.navigate(["/user"], { state: { email: this.user } });
      }
    }).catch(err=>{
      console.log(err)
    })
  }

  onAddUser(name: string, isAdmin: boolean) {
    this.store.dispatch(addUser({ name: name, isAdmin: isAdmin }));
  }
  onAdd(task: string, status: boolean) {
    this.store.dispatch(addTask({ task: task, status: status }));
  }
}
